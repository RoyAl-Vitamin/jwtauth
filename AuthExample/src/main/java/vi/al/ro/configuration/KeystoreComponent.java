package vi.al.ro.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

/**
 * Конфигурационный компонент, нужен для получения пары ключей
 */
@Configuration
//@PropertySource("classpath:application.properties")
public class KeystoreComponent {

    private static final Logger logger = LoggerFactory.getLogger(KeystoreComponent.class);

    @Bean
    public KeyPair getKeyPair(@Value("${keystore.password}") final String password,
                              @Value("${keystore.alias}") final String alias) {

        KeyPair keyPair = null;
        try {
            keyPair = getKeyPairInner(alias, password);
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | UnrecoverableKeyException e) {
            logger.error("ERROR: ", e);
            throw new RuntimeException(e);
        }
        return keyPair;
    }

    private KeyPair getKeyPairInner(String alias, String password) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException {
        char[] pwdArray = password.toCharArray();
        KeyStore ks = KeyStore.getInstance("JKS");
        InputStream in = getClass().getClassLoader().getResourceAsStream("keystore_file.jks");
        ks.load(in, pwdArray);

        Key key = ks.getKey(alias, pwdArray);
        if (key instanceof PrivateKey) {
            // Get certificate of public key
            Certificate cert = ks.getCertificate(alias);

            // Get public key
            PublicKey publicKey = cert.getPublicKey();

            // Return a key pair
            return new KeyPair(publicKey, (PrivateKey) key);
        }
        throw new RuntimeException("Cannot get Public and Private key");
    }
}
