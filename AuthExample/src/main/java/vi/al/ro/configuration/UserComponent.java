package vi.al.ro.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import vi.al.ro.interceptor.UserInterceptor;
import vi.al.ro.support.UserResolver;

import java.util.List;

/**
 * Add Resolver for read JWT before use
 */
@Configuration
public class UserComponent implements WebMvcConfigurer {

    private final UserInterceptor userInterceptor;

    // TODO Могут быть проблемы со сканированием пакета при старте
    private final UserResolver userResolver;

    public UserComponent(UserInterceptor userInterceptor, UserResolver userResolver) {
        this.userInterceptor = userInterceptor;
        this.userResolver = userResolver;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(userResolver);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(userInterceptor);
    }
}
