package vi.al.ro.support;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import vi.al.ro.component.User;

import java.security.KeyPair;

/**
 * Компонент, внедряющий в контроллеры {@link User} на основании полученного JWT из Header запроса
 */
@Component
public class UserResolver implements HandlerMethodArgumentResolver {

    private static final Logger logger = LoggerFactory.getLogger(UserResolver.class);

    private final static String AUTHORIZATION = "Authorization";

    private final KeyPair keyPair;

    public UserResolver(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().equals(User.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) {
        String JWSHeader = webRequest.getHeader(AUTHORIZATION);

        Jws<Claims> jws = Jwts.parser()
                .setSigningKey(keyPair.getPublic())
                .parseClaimsJws(JWSHeader);

        Integer userId = jws.getBody().get("user_id", Integer.class);
        String userName = jws.getBody().get("user_name", String.class);

        return new User(userId, userName);
    }
}
