package vi.al.ro.controller.auth;

import com.google.gson.Gson;

import javax.validation.constraints.NotNull;

public class AuthRequest {

    @NotNull
    public String login;

    @NotNull
    public String password;

    @Override
    public String toString() {
        // TODO посмотреть, что включено в Spring, заменить Gson на его
        return new Gson().toJson(this);
//        return "AuthRequest{" +
//                "login='" + login + '\'' +
//                ", password='" + password + '\'' +
//                '}';
    }
}
