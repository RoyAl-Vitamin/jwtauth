package vi.al.ro.controller.f;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import vi.al.ro.component.User;

/**
 * Пример контроллера с обзательной авторизацией
 */
@RestController
public class FController {

    private static final Logger logger = LoggerFactory.getLogger(FController.class);

    @RequestMapping(value = "/f", method = RequestMethod.POST, consumes = "application/json")
    public String auth(User user) {

        logger.info("user id = [{}], name = [{}]", user.id, user.name);

        return "Success!";
    }
}
