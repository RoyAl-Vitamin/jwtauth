package vi.al.ro.controller.auth;

import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.*;
import java.util.*;

/**
 * Контроллер возвращающий авторизационный токен
 */
@RestController
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    private static final Map<String, Object> HEADER_MAP = Map.of("alg", "RS256", "typ", "JWT");

    private final KeyPair keyPair;

    public AuthController(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    @RequestMapping(value = "/auth", method = RequestMethod.POST, consumes = "application/json")
    public String auth(@RequestBody @Valid final AuthRequest request) {

        Map<String, Object> claimsMap = getClaimsMap();

        // You use the private key (keyPair.getPrivate()) to create a JWS and the public key (keyPair.getPublic()) to parse/verify a JWS.
        String jws = Jwts.builder()
                .setHeader(HEADER_MAP)
                .setClaims(claimsMap)
                .signWith(keyPair.getPrivate())
                .compact();

        logger.info("JWT = [{}] for user with login = [{}] passwd = [{}]", jws, request.login, request.password);

        return jws;
    }

    private Map<String, Object> getClaimsMap() {
        Date nbf = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault()).getTime();

        Calendar calendar = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        calendar.add(Calendar.DAY_OF_MONTH, 5);
        Date exp = calendar.getTime();

        return Map.of("user_id", 1, "user_name", "superUser", "nbf", nbf, "exp", exp);
    }

}
