package vi.al.ro.controller;

import io.jsonwebtoken.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import vi.al.ro.configuration.KeystoreComponent;
import vi.al.ro.controller.auth.AuthRequest;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.KeyPair;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {KeystoreComponent.class})
@TestPropertySource(properties = {"keystore.password=D33193", "keystore.alias=CAPS"})
public class AuthTest {

    private static final Logger logger = LoggerFactory.getLogger(AuthTest.class);

    @Autowired
    KeyPair keyPair;

    @Test
    public void AuthTest1() {
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();

        AuthRequest aRequest = new AuthRequest();
        aRequest.login = "sysdba";
        aRequest.password = "pswd";
        String json = aRequest.toString();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth"))
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .build();

        HttpResponse<String> response = null;
        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            logger.error("ERROR", e);
            Assert.fail("Cannot get response");
            return;
        }

        logger.info("Response status code: " + response.statusCode());
        logger.info("Response headers: " + response.headers());
        logger.info("Response body: " + response.body());

        // Check signature
        String jwsString = response.body();
        try {
            Jws<Claims> jws = Jwts.parser()
                    .require("user_id", 1)
                    .require("user_name", "superUser")
                    .setSigningKey(keyPair.getPublic())
                    .parseClaimsJws(jwsString);

            // we can safely trust the JWT
            logger.info("we can safely trust the JWT");

            Integer userId = jws.getBody().get("user_id", Integer.class);
            logger.info("userId == [{}]", userId);
        } catch (InvalidClaimException ex) {
            String message = "Require claim not exec";
            logger.error(message);
            Assert.fail(message);
        } catch (JwtException e) {
            // we *cannot* use the JWT as intended by its creator
            String message = "We cannot use the JWT as intended by its creator";
            // May be token has expired
            logger.error(message);
            Assert.fail(message);
        }

        // Call func with user auth
        request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/f"))
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setHeader(HttpHeaders.AUTHORIZATION, jwsString)
                .POST(HttpRequest.BodyPublishers.noBody())
                .build();

        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            logger.error("ERROR", e);
            Assert.fail("Cannot get response 2");
        }

        logger.info("Response body: " + response.body());
        Assert.assertEquals(response.body(), "Success!");
    }
}
